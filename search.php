<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Tiptoe
 */

get_header(); ?>

	<main id="site-content" class="site-content" role="main">

		<?php
		if ( have_posts() ) :

			get_template_part( 'partials/page', 'header' );

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				* Run the loop for the search to output the results.
				* If you want to overload this in a child theme then include a file
				* called content-search.php and that will be used instead.
				*/
				get_template_part( 'content/content', 'search' );

			endwhile;

			get_template_part( 'partials/pagination', 'archive' );

		else :

			get_template_part( 'partials/content', 'none' );

		endif;
		?>

	</main><!-- #site-content -->

<?php
get_sidebar();
get_footer();
