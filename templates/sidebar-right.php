<?php
/**
 * Template Name: Sidebar Right
 *
 * Display the primary sidebar on the right.
 *
 * @package Tiptoe
 */

get_header(); ?>

	<main id="site-content" class="site-content" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'content/content', 'page' );

		endwhile;
		?>

	</main><!-- #site-content -->

<?php
get_sidebar();
get_footer();
