<?php
/**
 * Various small changes and filters for the theme.
 *
 * @package Tiptoe
 */

/**
 * Change .sticky class for sticky posts for compatibility with Foundation.
 *
 * @param array $classes
 * @return array
 */
function tiptoe_sticky_class( $classes ) {
	if ( ( $key = array_search( 'sticky', $classes, true ) ) !== false ) {
		unset( $classes[ $key ] );
		$classes[] = 'sticky-post';
	}

	return $classes;
}
add_filter( 'post_class', 'tiptoe_sticky_class', 20 );

/**
 * Change [...] to just "...".
 *
 * @return string $more
 */
function tiptoe_excerpt_more() {
	/* Translators: The &hellip; is the mark after the excerpt. */
	$more = esc_html__( '&hellip;', 'tiptoe' );

	return $more;
}
add_filter( 'excerpt_more', 'tiptoe_excerpt_more' );

/**
 * Setup some defaults for displaying our sidebar
 */
function tiptoe_display_sidebar_defaults() {
	if ( is_page_template( 'templates/sidebar-right.php' ) ) {
		return true;
	}
}
add_filter( 'tiptoe_display_sidebar', 'tiptoe_display_sidebar_defaults' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
 function tiptoe_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of has-sidebar when the sidebar is enabled and is active.
	if ( is_active_sidebar( 'sidebar-1' ) && tiptoe_display_sidebar() ) {
		$classes[] = 'has-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'tiptoe_body_classes' );
