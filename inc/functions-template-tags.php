<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Tiptoe
 */

/**
 * Outputs an entry's published date and time.
 *
 * @access public
 * @param  array $args
 * @return void
 */
function tiptoe_entry_published( $args = array() ) {
	echo tiptoe_get_entry_published( $args );
}

/**
 * Function for getting the current entry's published and modified date and time.
 *
 * @access public
 * @param  array $args
 * @return string
 */
function tiptoe_get_entry_published( $args = array() ) {
	$html = '';
	$defaults = array(
		'icon'   => '',
		'before' => '',
		'after'  => '',
	);
	$args = wp_parse_args( $args, $defaults );

	// Assign an icon if one is specified.
	$icon = $args['icon'] ? tiptoe_get_svg( $args['icon'] ) : '';

	$time = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time = sprintf(
		 $time,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$html .= '<span class="posted-on">' . $icon . $args['before'] . '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time . '</a>' . $args['after'] . '</span>';

	return $html;
}

/**
 * Outputs an entry's author.
 *
 * @access public
 * @param  array $args
 * @return void
 */
function tiptoe_entry_author( $args = array() ) {
	echo tiptoe_get_entry_author( $args );
}

/**
 * Function for getting the current entry's author in The Loop and linking to the author archive page.
 *
 * @access public
 * @param  array $args
 * @return string
 */
function tiptoe_get_entry_author( $args = array() ) {
	$html = '';
	$defaults = array(
		'text'   => '%s',
		'icon'   => '',
		'before' => '',
		'after'  => '',
	);
	$args = wp_parse_args( $args, $defaults );

	// Assign an icon if one is specified.
	$icon = $args['icon'] ? tiptoe_get_svg( $args['icon'] ) : '';

	$html .= '<span class="entry-author">' . $icon . $args['before'];
	$html .= sprintf( $args['text'], '<a class="entry-author-link url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>' );
	$html .= $args['after'] . '</span>';

	return $html;
}

/**
 * Custom comments popup link.
 * Allows the use of an icon and optionally hide the text.
 */
function tiptoe_comments_popup_link( $args = array() ) {
	$defaults = array(
		'icon'   => '',
		'text'   => true,
		'zero'   => esc_html_x( 'No Comments', 'no comments text', 'tiptoe' ),
		'one'    => esc_html_x( '1 Comment',  'single comment text', 'tiptoe' ),
		'more'   => esc_html_x( '% Comments', 'multiple comments text', 'tiptoe' ),
		'class'  => 'comment-link',
		'none'   => '',
	);
	$args   = wp_parse_args( $args, $defaults );
	$id     = get_the_ID();
	$title  = get_the_title();
	$number = get_comments_number( $id );

	// Assign an icon if one is specified.
	$icon = $args['icon'] ? tiptoe_get_svg( $args['icon'] ) : '';

	// If comments are closed and we have no comments, end early.
	if ( 0 === $number && ! comments_open() ) {
		return;
	}

		// If we are using text.
		if ( true === $args['text'] ) {
			$zero = $icon . sprintf( __( '%1$s<span class="screen-reader-text"> on %2$s</span>', 'tiptoe' ), $args['zero'], $title );
			$one  = $icon . sprintf( __( '%1$s<span class="screen-reader-text"> on %2$s</span>', 'tiptoe' ), $args['one'], number_format_i18n( $number ), $title );
			$more = $icon . sprintf( __( '%1$s<span class="screen-reader-text"> on %2$s</span>', 'tiptoe' ), $args['more'], number_format_i18n( $number ), $title );
		} else {
			$zero = $icon . sprintf( __( '%1$s<span class="screen-reader-text"> Comments on %2$s</span>', 'tiptoe' ), number_format_i18n( $number ), $title );
			$one  = $icon . sprintf( __( '%1$s<span class="screen-reader-text"> Comment on %2$s</span>', 'tiptoe' ), number_format_i18n( $number ), $title );
			$more = $icon . sprintf( __( '%1$s<span class="screen-reader-text"> Comments on %2$s</span>', 'tiptoe' ), number_format_i18n( $number ), $title );
		}

	comments_popup_link( $zero, $one, $more, $args['class'], $args['none'] );
}

/**
 * Outputs a post's taxonomy terms.
 *
 * @access public
 * @param array $args
 * @return void
 */
function tiptoe_post_terms( $args = array() ) {
	echo tiptoe_get_post_terms( $args ); // WPCS: XSS ok.
}

/**
 * This template tag is meant to replace template tags like `the_category()`, `the_terms()`, etc.  These core
 * WordPress template tags don't offer proper translation and RTL support without having to write a lot of
 * messy code within the theme's templates.  This is why theme developers often have to resort to custom
 * functions to handle this (even the default WordPress themes do this). Particularly, the core functions
 * don't allow for theme developers to add the terms as placeholders in the accompanying text (ex: "Posted in %s").
 * This funcion is a wrapper for the WordPress `get_the_terms_list()` function.  It uses that to build a
 * better post terms list.
 *
 * @author  Justin Tadlock
 * @link    https://github.com/justintadlock/hybrid-core/blob/2.0/functions/template-post.php
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * @param array $args
 * @return string
 */
function tiptoe_get_post_terms( $args = array() ) {
	$html = '';
	$defaults = array(
		'post_id'    => get_the_ID(),
		'taxonomy'   => 'category',
		'text'       => '%s',
		'before'     => '',
		'after'      => '',
		'items_wrap' => '<span %s>%s%s</span>',
		/* Translators: Separates tags, categories, etc. when displaying a post. */
		'sep'        => '<span class="sep">' . esc_html_x( ', ', 'taxonomy terms separator', 'tiptoe' ) . '</span>',
		'icon'       => '',
	);
	$args = wp_parse_args( $args, $defaults );
	$terms = get_the_term_list( $args['post_id'], $args['taxonomy'], '', $args['sep'], '' );
	$icon  = $args['icon'] ? tiptoe_get_svg( $args['icon'] ) : '';
	if ( ! empty( $terms ) ) {
		$html .= $args['before'];
		$html .= sprintf( $args['items_wrap'], 'class="entry-terms ' . $args['taxonomy'] . '"', $icon, sprintf( $args['text'], $terms ) );
		$html .= $args['after'];
	}

	return $html;
}

/**
 * Outputs read more link.
 *
 * @param array $args
 * @return void
 */
function tiptoe_read_more_link( $args = array() ) {
	echo tiptoe_get_read_more_link( $args );
}

/**
 * Read more link.
 *
 * @param array $args
 * @return string
 */
function tiptoe_get_read_more_link( $args = array() ) {
	$defaults = array(
		'text'  => esc_html_x( 'Read more', 'read more text', 'tiptoe' ),
		'class' => '',
		'icon'  => '',
	);
	$args = wp_parse_args( $args, $defaults );

	/* Translators: The $2%s is the post title shown to screen readers. */
	$text  = sprintf( esc_attr__( '%1$s %2$s', 'tiptoe' ), $args['text'], '<span class="screen-reader-text">' . get_the_title() ) . '</span>';
	$class = $args['class'] ? ' ' . $args['class'] : '';
	$icon  = $args['icon'] ? tiptoe_get_svg( $args['icon'] ) : '';
	$more  = sprintf( '<a href="%s" class="more-link' . $class . '">%s%s</a>', esc_url( get_permalink() ), $text, $icon );

	return $more;
}

/**
 * Outputs site copyright text.
 * use filter `tiptoe_copyright_text` to change this in a child theme.
 */
function tiptoe_copyright() {
	/* Translators: 1 is the year, 2 is the site name. */
	$copyright = sprintf( esc_attr__( 'Copyright &#169; %1$s %2$s.', 'tiptoe' ), date_i18n( 'Y' ), get_bloginfo( 'name' ) );
	echo '<span class="copyright">' . apply_filters( 'tiptoe_copyright_text', $copyright ) . '</span>';  // WPCS: XSS ok.
}

/**
 * Outputs the theme credit link.
 * use filter `tiptoe_credit_text` to change this in a child theme.
 */
function tiptoe_credit() {
	if ( apply_filters( 'tiptoe_credit_text', true ) ) {
		/* Translators: 1 is the theme name, 2 is theme author link. */
		$credit = sprintf( __( 'Theme: %1$s by %2$s.', 'tiptoe' ), 'Tiptoe', '<a href="http://brettmason.co.uk" rel="author">Brett Mason</a>' );
		echo '<span class="credit">' . apply_filters( 'tiptoe_credit_text', $credit ) . '</span>'; // WPCS: XSS ok.
	}
}

/**
 * A custom menu walker for the Foundation menu structure.
 */
class Foundation_Menu_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul class=\"menu vertical submenu\">\n";
	}
}

/**
 * A simple function to check if we should display the primary sidebar.
 * Use filter `tiptoe_display_sidebar` to change this in a child theme.
 *
 * @return bool
 */
function tiptoe_display_sidebar() {
	return apply_filters( 'tiptoe_display_sidebar', false );
}
