<?php
/**
 * Functions for dealing with embeds.
 *
 * @package Tiptoe
 */

/**
 * Checks embed URL patterns to see if they should be wrapped in some special HTML, particularly
 * for responsive videos.
 *
 * @return string
 */
function tiptoe_maybe_wrap_embed( $html, $url ) {
	if ( ! $html || ! is_string( $html ) || ! $url ) {
		return $html;
	}

	$do_wrap = false;
	$patterns = array(
		'#http://((m|www)\.)?youtube\.com/watch.*#i',
		'#https://((m|www)\.)?youtube\.com/watch.*#i',
		'#http://((m|www)\.)?youtube\.com/playlist.*#i',
		'#https://((m|www)\.)?youtube\.com/playlist.*#i',
		'#http://youtu\.be/.*#i',
		'#https://youtu\.be/.*#i',
		'#https?://(.+\.)?vimeo\.com/.*#i',
		'#https?://(www\.)?dailymotion\.com/.*#i',
		'#https?://dai.ly/*#i',
		'#https?://(www\.)?hulu\.com/watch/.*#i',
		'#https?://wordpress.tv/.*#i',
		'#https?://(www\.)?funnyordie\.com/videos/.*#i',
		'#https?://vine.co/v/.*#i',
		'#https?://(www\.)?collegehumor\.com/video/.*#i',
		'#https?://(www\.|embed\.)?ted\.com/talks/.*#i',
	);
	$patterns = apply_filters( 'tiptoe_maybe_wrap_embed_patterns', $patterns );
	foreach ( $patterns as $pattern ) {
		$do_wrap = preg_match( $pattern, $url );

		if ( $do_wrap ) {
			return tiptoe_wrap_embed_html( $html );
		}
	}

	return $html;
}
add_filter( 'embed_oembed_html', 'tiptoe_maybe_wrap_embed', 10, 2 );
add_filter( 'embed_handler_html', 'tiptoe_maybe_wrap_embed', 10, 2 );

/**
 * Adds a wrapper to embeds.
 * Allows the embed to be responsive with some extra styling.
 *
 * @return string
 */
function tiptoe_wrap_embed_html( $html ) {
	if ( empty( $html ) || ! is_string( $html ) ) {
		return $html;
	}

	preg_match( '/width=[\'"](.+?)[\'"]/i',  $html, $width_matches );
	preg_match( '/height=[\'"](.+?)[\'"]/i', $html, $height_matches );
	$width  = ! empty( $width_matches ) && isset( $width_matches[1] ) ? absint( $width_matches[1] ) : 0;
	$height = ! empty( $height_matches ) && isset( $height_matches[1] ) ? absint( $height_matches[1] ) : 0;
	$ratio = tiptoe_get_ratio( $width, $height );

	return sprintf( '<div class="responsive-embed%s">%s</div>', $ratio, $html );
}

/**
 * Utility function for adding a class to embeds, depending on the ratio of the embed.
 *
 * @return string
 */
function tiptoe_get_ratio( $width, $height ) {
	$ratio = '';
	$calc = array(
		' widescreen' => array( 'w' => 16, 'h' => 9 ),
		' panorama'   => array( 'w' => 256, 'h' => 81 ),
	);
	$calc = apply_filters( 'tiptoe_embed_ratios', $calc );
	foreach ( $calc as $name => $dim ) {
		if ( ( $width / $dim['w'] * $dim['h'] ) === $height ) {
			$ratio = $name;
			break;
		}
	}

	return $ratio;
}
