<?php
/**
 * Filters and actions to clean up theme output.
 *
 * @package Tiptoe
 */

/**
 * Cleanup WordPress <head> and a few other areas.
 */
function tiptoe_head_cleanup() {
	// Display the link to the Really Simple Discovery service endpoint, EditURI link.
	remove_action( 'wp_head', 'rsd_link' );

	// Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'wlwmanifest_link' );

	// Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );

	// Display the WordPress version.
	remove_action( 'wp_head', 'wp_generator' );

	// Adds a “shortlink” into your head that will look like http://example.com/?p=ID.
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );

	// Remove emojis.
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'emoji_svg_url', '__return_false' );

	// Remove injected CSS from Recent Comments Widget.
	global $wp_widget_factory;
	if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}
add_action( 'init', 'tiptoe_head_cleanup' );
