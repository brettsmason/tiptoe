<?php
/**
 * Tiptoe Comments
 *
 * @package Tiptoe
 */

if ( ! function_exists( 'tiptoe_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @param  array $comment    The current comment object.
 * @param  array $args       The comment configuration arguments.
 * @param  mixed $depth      Depth of the current comment.
 *
 * @return void
 */
function tiptoe_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' === $comment->comment_type || 'trackback' === $comment->comment_type ) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php esc_html_e( 'Pingback:', 'tiptoe' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( 'Edit', 'tiptoe' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

	<?php else : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'comment-parent' ); ?>>
		<div class="comment-wrap">

			<?php if ( 0 !== $args['avatar_size'] ) : ?>
				<aside class="comment-avatar">
					<?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
				</aside>
			<?php endif; ?>

			<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
				<header class="comment-header">
					<cite class="comment-author"><?php comment_author_link(); ?></cite>

					<?php if ( in_array( 'bypostauthor', get_comment_class(), true ) ) : ?>
						<span class="comment-post-author"><?php esc_html_e( 'Author', 'tiptoe' ); ?></span>
					<?php endif; ?>

					<span class="comment-published">
						<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
							<time datetime="<?php comment_time( 'c' ); ?>">
								<?php printf( esc_html__( '%s ago', 'tiptoe' ), human_time_diff( get_comment_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
							</time>
						</a>
					</span>

					<?php if ( '0' === $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'tiptoe' ); ?></p>
					<?php endif; ?>
				</header>

				<div class="comment-content">
					<?php comment_text(); ?>
				</div>

				<?php
				comment_reply_link( array_merge( $args, array(
					'add_below' => 'div-comment',
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
					'before'    => '<footer class="comment-reply">',
					'after'     => '</footer>',
				) ) );
				?>
			</div>
		</article>

	<?php
	endif;
}
endif;
