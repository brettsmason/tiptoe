<?php
/**
 * SVG icon related functions and filters.
 *
 * @package Tiptoe
 */

/**
 * Add SVG definitions to the footer.
 */
function tiptoe_include_svg_icons() {
	// Define SVG sprite file.
	$svg_icons = get_parent_theme_file_path( '/assets/icons/icons.svg' );
	// If it exists, include it.
	if ( file_exists( $svg_icons ) ) {
		require_once( $svg_icons );
	}
}
add_action( 'wp_footer', 'tiptoe_include_svg_icons', 9999 );

/**
 * Return SVG markup.
 *
 * @param  string $icon Required. Use the icon filename, e.g. "facebook-square".
 * @param  array  $args {
 *     Additional parameters used to customise the SVG.
 *
 *     @param string  $title Optional. SVG title, e.g. "Facebook".
 *     @param string  $desc Optional. SVG description, e.g. "Share this post on Facebook".
 *     @param string  $class Optional. Class to apply to the SVG. Defaults to `icon icon-{icon-name}`.
 *     @param Boolean $inline Optional. Output the SVG inline or not. Defaults to false.
 * }
 * @return string SVG markup.
 */
function tiptoe_get_svg( $icon, $args = array() ) {

	// Define an icon.
	if ( false === $icon ) {
		return esc_html__( 'Please define an SVG icon filename.', 'tiptoe' );
	}

	// Make sure $args is an array.
	if ( ! is_array( $args ) ) {
		return esc_html__( 'Please define optional parameters in the form of an array.', 'tiptoe' );
	}

	// Set defaults.
	$defaults = array(
		'title'       => '',
		'desc'        => '',
		'aria_hidden' => false, // Hide from screen readers.
		'class'  => '',
	);

	// Parse args.
	$args = wp_parse_args( $args, $defaults );

	// Sets unique IDs for use by aria-labelledby.
	$title_id = $args['title'] ? uniqid( 'title-' ) : '';
	$desc_id = $args['desc'] ? uniqid( 'desc-' ) : '';

	// Sets SVG title.
	$title = $args['title'] ? '<title id="' . $title_id . '">' . esc_html( $args['title'] ) . '</title>' : '';

	// Sets SVG desc.
	$desc = $args['desc'] ? '<desc id="' . $desc_id . '">' . esc_html( $args['desc'] ) . '</desc>' : '';

	// Set ARIA labelledby.
	if ( $args['title'] && $args['desc'] ) {
		$aria_labelledby = 'aria-labelledby="' . $title_id . ' ' . $desc_id . '"';
	} elseif ( $args['title'] ) {
		$aria_labelledby = 'aria-labelledby="' . $title_id . '"';
	} elseif ( $args['desc'] ) {
		$aria_labelledby = 'aria-labelledby="' . $desc_id . '"';
	} else {
		$aria_labelledby = '';
	}

	// Set ARIA hidden.
	if ( $args['title'] || $args['desc'] ) {
		$aria_hidden = '';
	} else {
		$aria_hidden = 'aria-hidden="true"';
	}

	// Sets icon class.
	$class = $args['class'] ? esc_html( $args['class'] ) : 'icon icon-' . esc_html( $icon );

	// Begin SVG markup.
	$svg = '<svg class="' . $class . '"' . $aria_hidden . $aria_labelledby . ' role="img">';

	// If there is a title, display it.
	if ( $title ) {
		$svg .= '<title  id="' . $title_id . '">' . esc_html( $args['title'] ) . '</title>';
	}

	// If there is a description, display it.
	if ( $desc ) {
		$svg .= '<desc id="' . $desc_id . '">' . esc_html( $args['desc'] ) . '</desc>';
	}

	// Use absolute path in the Customizer so that icons show up in there.
	if ( is_customize_preview() ) {
		$svg .= '<use xlink:href="' . get_theme_file_uri() . 'assets/icons/icons.svg' . '#icon-' . esc_html( $icon ) . '"></use>';
	} else {
		$svg .= '<use xlink:href="#icon-' . esc_html( $icon ) . '"></use>';
	}

	$svg .= '</svg>';

	return $svg;
}

/**
 * Display an SVG icon.
 *
 * @param  array $args Parameters needed to display an SVG.
 */
function tiptoe_do_svg( $icon, $args = array() ) {
	echo tiptoe_get_svg( $icon, $args );
}
