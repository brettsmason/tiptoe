<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Tiptoe
 */

?>

	</div><!-- #site-main -->

	<?php get_template_part( 'partials/sidebar', 'subsidiary' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-footer-wrap">
			<div class="site-info">
				<?php tiptoe_copyright(); ?>
				<?php tiptoe_credit(); ?>
			</div><!-- .site-info -->
		</div><!-- .site-footer-wrap -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
