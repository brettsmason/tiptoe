<?php
/**
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @package Tiptoe
 */

// Load theme-specific files.
require_once( get_template_directory() . '/inc/functions-cleanup.php' );
require_once( get_template_directory() . '/inc/functions-comments.php' );
require_once( get_template_directory() . '/inc/functions-customizer.php' );
require_once( get_template_directory() . '/inc/functions-embeds.php' );
require_once( get_template_directory() . '/inc/functions-extras.php' );
require_once( get_template_directory() . '/inc/functions-filters.php' );
require_once( get_template_directory() . '/inc/functions-icons.php' );
require_once( get_template_directory() . '/inc/functions-template-tags.php' );

// Conditionally load Jetpack compatibility file.
if ( defined( 'JETPACK__VERSION' ) ) {
	require_once( get_template_directory() . '/inc/functions-jetpack.php' );
}

/**
 * The theme setup function.
 *
 * @access public
 * @return void
 */
function tiptoe_theme_setup() {

	// Make theme available for translation.
	load_theme_textdomain( 'tiptoe', get_template_directory() . '/languages' );

	// Automatically add feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// Let WordPress manage the document title.
	add_theme_support( 'title-tag' );

	// Enable support for Post Thumbnails.
	add_theme_support( 'post-thumbnails' );

	// Switch to HTML markup for search form, comment form, and comments.
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add excerpt to pages.
	add_post_type_support( 'page', 'excerpt' );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'tiptoe_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Site logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 500,
		'height'      => 200,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'tiptoe_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 */
function tiptoe_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tiptoe_content_width', 1200 );
}
add_action( 'after_setup_theme', 'tiptoe_content_width', 0 );

/**
 * Register menus.
 */
function tiptoe_register_menus() {
	register_nav_menus( array(
		'primary'   => esc_html__( 'Primary', 'tiptoe' ),
		'social'    => esc_html__( 'Social', 'tiptoe' ),
	) );
}
add_action( 'after_setup_theme', 'tiptoe_register_menus' );

/**
 * Register widget areas.
 */
function tiptoe_widgets() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tiptoe' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'tiptoe' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="widget-wrap">',
		'after_widget'  => '</div></section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'tiptoe' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Widget area for the footer.', 'tiptoe' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="widget-wrap">',
		'after_widget'  => '</div></section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'tiptoe_widgets' );

/**
 * Enqueue scripts and styles.
 */
function tiptoe_scripts() {

	// Add parent theme styles if using child theme.
	if ( is_child_theme() ) {
		wp_enqueue_style( 'tiptoe-parent-style', get_template_directory_uri() . '/assets/css/main.css', array(), null );
	}

	// Load active theme stylesheet.
	wp_enqueue_style( 'tiptoe-style', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), null );

	// Load custom fonts.
	// wp_enqueue_style( 'tiptoe-fonts', '//fonts.googleapis.com/css?family=Montserrat|Istok+Web' );

	// Register scripts.
	wp_enqueue_script( 'tiptoe-scripts', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), null, true );

	// Load comments script.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tiptoe_scripts' );
