# Tiptoe

Tiptoe is a starter-theme for WordPress based on Foundation (6), the most advanced responsive framework in the world. Tiptoe is meant to help you build an awesome theme, but is not designed to be the final product.

## Requirements

**Tiptoe requires [Node.js](http://nodejs.org) to be installed on your machine.** Please be aware that you might encounter problems with the installation if you are using the most current Node version (bleeding edge) with all the latest features.

**[Yarn](https://yarnpkg.com) is also highly recommended as a replacement for NPM as in my experience its a lot faster.** The instructions assume you are using Yarn, but they should be similar if using NPM.

## Quickstart

### 1. Clone the repository and install with yarn
```bash
$ cd my-wordpress-folder/wp-content/themes/
$ git clone https://brettsmason@bitbucket.org/brettsmason/tiptoe.git
$ cd tiptoe
$ yarn
```

### 2. Get started

For local WordPress development I recommend [Local by Flywheel](https://local.getflywheel.com/), but feel free to use whatever you prefer.

Then run:

```bash
$ yarn build
```
Alternatively you can also run Browsersync and watch your files for changes:

```bash
$ yarn serve
```

You can also lint your Sass/JavaScript for errors using:

```bash
$ yarn lint
```
**If you want to take advantage of Browsersync, make sure you open `gulpfile.babel.js` and put your local development server address (e.g http://mywpsite.dev) in the `URL` variable.**

### 3. Compile assets for production

When building for production, the CSS and JS will be minified. To minify the assets in your `/assets` folder, run

```bash
$ yarn prod
```

### Project structure

In the `/src` folder you will the working files for all your assets. Every time you make a change to a file that is watched by Gulp, the output will be saved to the `/assets` folder. The contents of this folder is the compiled code that you should not touch (unless you have a good reason for it).

The `/templates` folder contains templates that can be selected in the Pages section of the WordPress admin panel. To create a new page-template, simply create a new file in this folder and make sure to give it a template name.

### Styles and Sass Compilation

 * `style.css`: Simply change your theme details then don't worry about this every again.

 * `src/scss/main.scss`: Main theme stylesheet
 * `src/scss/base/*.scss`: Global settings
 * `src/scss/components/*.scss`: Buttons etc.
 * `src/scss/modules/*.scss`: Topbar, footer etc.
 * `src/scss/views/*.scss`: Specific view (eg page templates) styling.

 * `assets/css/main.css`: This file is loaded in the `<head>` section of your document, and contains the compiled styles for your project.

### JavaScript Compilation

All JavaScript files in the `src/js` folder, along with Foundation and its dependencies, are bundled into one file called `main.js`. The files are imported using module dependency with [webpack](https://webpack.js.org/) as the module bundler.

If you're unfamiliar with modules and module bundling, check out [this resource for node style require/exports](http://openmymind.net/2012/2/3/Node-Require-and-Exports/) and [this resource to understand ES6 modules](http://exploringjs.com/es6/ch_modules.html).

Foundation modules are loaded in the `src/js/lib/foundation.js` file. By default all components are loaded. You can also pick and choose which modules to include. Just follow the instructions in the file.

## Documentation

* [Zurb Foundation Docs](http://foundation.zurb.com/docs/)
* [WordPress Codex](http://codex.wordpress.org/)

## Contributing
#### Here are ways to get involved:

1. [Star](https://bitbucket.org/brettsmason/tiptoe) the project
2. Answer questions that come through [GitHub issues](https://bitbucket.org/brettsmason/tiptoe/issues)
3. Report a bug that you find
4. Share a theme you've built on top of Tiptoe

#### Pull Requests

Pull requests are highly appreciated. Please follow these guidelines:

1. Solve a problem. Features are great, but even better is cleaning-up and fixing issues in the code that you discover
2. Make sure that your code is bug-free and does not introduce new bugs
3. Create a [pull request](https://help.github.com/articles/creating-a-pull-request)
4. Verify that all the Travis-CI build checks have passed

## Copyright and License

The following resources are included or used in part within the theme package.
* [Foundation](http://foundation.zurb.com) by Zurb, Inc. - Licensed under the [MIT License](https://opensource.org/licenses/MIT).
* Based on [Underscores](http://underscores.me/) by Automattic, Inc. - Licensed under the [GPL, version 2 or later](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html).

All other resources and theme elements are licensed under the GNU GPL, version 2 or later.

2017 &copy; Brett Mason.
