<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Tiptoe
 */

get_header(); ?>

	<main id="site-content" class="site-content" role="main">

		<?php
		while ( have_posts() ) : the_post();

			/*
			* Include the Post-Type-specific template for the content.
			* If you want to override this in a child theme, then include a file
			* called content-single-___.php (where ___ is the Post Type name) and that will be used instead.
			*/
			get_template_part( 'content/content-single', get_post_type() );

			get_template_part( 'partials/pagination', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

	</main><!-- #site-content -->

<?php
get_sidebar();
get_footer();
