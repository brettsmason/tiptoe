import foundation from './lib/foundation';

// Initialize Foundation
$(document).foundation();

// Include svg4everybody
import svg4everybody from 'svg4everybody';
svg4everybody({polyfill: false});

// Add class to body when off canvas is opened
$('[data-off-canvas]').on('opened.zf.offcanvas', function() {
	$('body').addClass('is-menu-open');
});

// Remove class from body when off canvas is closed
$('[data-off-canvas]').on('closed.zf.offcanvas', function() {
	$('body').removeClass('is-menu-open');
});
