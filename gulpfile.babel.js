import {src, dest, watch, parallel, series, lastRun} from 'gulp';
import del from 'del';
import sass from 'gulp-sass';
import cleanCss from 'gulp-clean-css';
import gulpif from 'gulp-if';
import babel from 'gulp-babel';
import yargs from 'yargs';
import browserSync from 'browser-sync';
import cheerio from 'gulp-cheerio';
import svgmin from 'gulp-svgmin';
import svgstore from 'gulp-svgstore';
import rename from 'gulp-rename';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import named from 'vinyl-named';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';
import wpPot from 'gulp-wp-pot';
import eslint from 'gulp-eslint';
import sassLint from 'gulp-sass-lint';

// Set project paths
const paths = {
  styles: {
    src: 'src/scss/**/*.scss',
    dist: 'assets/css',
    includePaths: ['node_modules/foundation-sites/scss', 'node_modules/motion-ui/src', 'node_modules/nth-grid/sass']
  },
  scripts: {
    src: 'src/js/*.js',
    dist: 'assets/js'
  },
  icons: {
    src: 'src/icons/*.svg',
    dist: 'assets/icons'
  },
  assets: {
    src: ['src/**/*', '!src/{scss,js,icons}{,/**/*}'],
    dist: 'assets'
  }
}


// Recognise `--production` argument
const argv = yargs.argv;
const production = !!argv.production;


/**
 * Compile Sass and run stylesheet through Autoprefixer and minify.
 *
 * https://www.npmjs.com/package/gulp-sass
 * https://www.npmjs.com/package/gulp-autoprefixer
 * https://www.npmjs.com/package/gulp-clean-css
 */
export const buildStyles = () => src(paths.styles.src)
  .pipe(sourcemaps.init())
  .pipe(sass.sync({
    includePaths: paths.styles.includePaths,
    outputStyle: 'expanded'
  })
  .on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: [
      'last 2 versions',
      'ie >= 10',
      'ios >= 7'
    ]
  }))
  .pipe(gulpif(production, cleanCss({ compatibility: 'ie10' })))
  .pipe(gulpif(!production, sourcemaps.write('./')))
  .pipe(dest(paths.styles.dist))
  .pipe(browserSync.stream());


/**
 * Bundle, transform and minify JavaScript.
 *
 * https://www.npmjs.com/package/vinyl-named
 * https://www.npmjs.com/package/webpack
 * https://www.npmjs.com/package/webpack-stream
 * https://www.npmjs.com/package/gulp-uglify
 */
export const buildScripts = () => src(paths.scripts.src)
  .pipe(named())
  .pipe(sourcemaps.init())
  .pipe(webpackStream({
	  externals: {
		  jquery: 'jQuery'
	  },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: [/(node_modules)(?![/|\\](foundation-sites))/],
          use: {
            loader: 'babel-loader'
          }
        }
      ]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		})
	]
  }, webpack))
  .pipe(gulpif(production, uglify()
    .on('error', e => { console.log(e); })
  ))
  .pipe(gulpif(!production, sourcemaps.write()))
  .pipe(dest(paths.scripts.dist))
  .pipe(browserSync.stream());


/**
 * Sass linting.
 *
 * https://www.npmjs.com/package/sass-lint
 */
export const lintStyles = () => src(paths.styles.src)
	.pipe(sassLint())
	.pipe(sassLint.format())
	.pipe(sassLint.failOnError());


/**
 * JavaScript linting.
 *
 * https://www.npmjs.com/package/gulp-eslint
 */
export const lintScripts = () => src(paths.scripts.src)
	.pipe(eslint())
	.pipe(eslint.format())
	.pipe(eslint.failAfterError());


/**
 * Minify, concatenate, and clean SVG icons.
 *
 * https://www.npmjs.com/package/gulp-svgmin
 * https://www.npmjs.com/package/gulp-svgstore
 * https://www.npmjs.com/package/gulp-cheerio
 */
export const buildIcons = () => src(paths.icons.src)
  .pipe(svgmin())
  .pipe(rename({ 'prefix': 'icon-' }))
  .pipe(svgstore({'inlineSvg': true}))
  .pipe(cheerio({
    'run': function($, file) {
      $('svg').attr('style', 'display:none');
      $('[fill]').removeAttr('fill');
    },
    'parserOptions': {'xmlMode': true}
  }))
  .pipe(dest(paths.icons.dist))
  .pipe(browserSync.stream());


/**
 * Copy other static assets.
 */
export const copyAssets = () => src(paths.assets.src)
.pipe(dest(paths.assets.dist));


/**
 * Scan the theme and create a POT file.
 *
 * https://www.npmjs.com/package/gulp-wp-pot
 */
export const translate = () => src('**/*.php')
  .pipe(wpPot({
    domain: 'tiptoe',
    package: 'Tiptoe'
  }))
  .pipe(dest('languages/tiptoe.pot'))
  .pipe(browserSync.stream());


// Clean
export const clean = () => del(['assets']);


// Watch Task
export const watchFiles = () => {
  watch('**/*.php', series(browserSync.reload));
  watch(paths.styles.src, series(buildStyles));
  watch(paths.scripts.src, series(buildScripts));
  watch(paths.icons.src, series(buildIcons));
  watch(paths.assets.src, series(copyAssets));
};


// Start BrowserSync server
function server(done) {
  browserSync.init({
    proxy: 'theme-development.dev'
  });
  done();
}

// Build Task
export const build = series(clean, parallel(buildStyles, buildScripts, buildIcons, copyAssets, translate));

// Serve Task
export const serve = series(clean, parallel(buildStyles, buildScripts, buildIcons, copyAssets), parallel(watchFiles, server));

// Lint Task
export const lint = series(lintStyles, lintScripts);

// Default task
export default build;
