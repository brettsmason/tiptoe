<?php
/**
 * Template part for displaying post navigation on a single post.
 *
 * @link https://codex.wordpress.org/Function_Reference/previous_post_link
 * @link https://codex.wordpress.org/Function_Reference/next_post_link
 *
 * @package Tiptoe
 */

?>

<nav class="pager-container">
	<ul class="pager" role="navigation" aria-label="Pagination">
		<?php previous_post_link( '<li class="pager-item pager-prev"><span class="pager-item-title">' . esc_html_x( 'Previous:', 'previous post navigation', 'tiptoe' ) . '</span>%link</li>' ); ?>
		<?php next_post_link( '<li class="pager-item pager-next"><span class="pager-item-title">' . esc_html_x( 'Next:', 'next post navigation', 'tiptoe' ) . '</span>%link</li>' ); ?>
	</ul>
</nav><!-- .pager-container -->
