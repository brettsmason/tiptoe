<?php
/**
 * Template part for displaying posts navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/the_posts_pagination
 *
 * @package Tiptoe
 */

the_posts_pagination(
	array(
		'mid_size'           => 2,
		'prev_text'          => esc_html_x( 'Previous', 'posts navigation', 'tiptoe' ),
		'next_text'          => esc_html_x( 'Next',     'posts navigation', 'tiptoe' ),
		'before_page_number' => '<span class="screen-reader-text">' . __( 'Page', 'tiptoe' ) . ' </span>',
		'type'               => 'list',
	)
);
