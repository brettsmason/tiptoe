<?php
/**
 * Template part for displaying a page header
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tiptoe
 */

?>

<header class="page-header">

	<?php if ( is_home() && ! is_front_page() ) : ?>
		<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
	<?php elseif ( is_search() ) : ?>
		<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'tiptoe' ), '<span class="search-term">' . get_search_query() . '</span>' ); ?></h1>
	<?php elseif ( is_404() ) : ?>
		<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'tiptoe' ); ?></h1>
	<?php else : ?>
		<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
	<?php endif; ?>

	<?php
	if ( ! is_paged() && get_the_archive_description() ) {
		the_archive_description( '<div class="page-description">', '</div>' );
	}
	?>

</header><!-- .page-header -->
