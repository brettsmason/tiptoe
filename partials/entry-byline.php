<?php
/**
 * Template part for displaying the entry byline.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tiptoe
 */

?>

<div class="entry-byline">
	<?php
		tiptoe_entry_published();

		tiptoe_entry_author(
			array(
				'text' => esc_html_x( ' by %s', 'post author', 'tiptoe' ),
			)
		);

		tiptoe_comments_popup_link(
			array(
				'text' => true,
			)
		);
	?>
</div><!-- .entry-byline -->
