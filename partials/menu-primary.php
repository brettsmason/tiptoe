<?php
/**
 * Template part for displaying the primary menu
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tiptoe
 */

?>

<div class="menu-toggle">
	<button class="menu-toggle-button" data-toggle="navigation-primary">
		<?php
		tiptoe_do_svg(
			'hamburger', array(
				'title' => esc_html( 'Toggle Menu', 'tiptoe' ),
				'inline' => true
			)
		);
		?>
	</button>
</div>

<nav id="navigation-primary" class="navigation-primary reveal-for-large" role="navigation" data-off-canvas data-reveal-on="large" data-content-scroll="false">
	<div class="navigation-primary-wrap">
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'primary',
				'container'      => '',
				'menu_id'        => 'navigation-primary',
				'menu_class'     => 'navigation-primary-menu menu vertical large-horizontal',
				'fallback_cb'    => '',
				'items_wrap'     => '<ul id="%s" class="%s" data-responsive-menu="accordion large-dropdown" data-submenu-toggle="true">%s</ul>',
				'walker'         => new Foundation_Menu_Walker(),
			)
		);
		?>
	</div><!-- .navigation-primary-wrap -->

	<button class="close-button" aria-label="Close menu" type="button" data-close>
		<span aria-hidden="true">&times;</span>
	</button>
</nav><!-- .navigation-primary -->
