<?php
/**
 * The sidebar containing the footer widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Tiptoe
 */

if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}
?>

<aside id="sidebar-subsidiary" class="sidebar-subsidiary" role="complementary">
	<div class="sidebar-subsidiary-wrap wrapper">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- .sidebar-subsidiary-wrap -->
</aside><!-- #sidebar-subsidiary -->
