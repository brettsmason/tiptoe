# Copyright (C) 2017 Tiptoe
# This file is distributed under the same license as the Tiptoe package.
msgid ""
msgstr ""
"Project-Id-Version: Tiptoe\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: 404.php:19
msgid "It looks like nothing was found at this location. Maybe try a search?"
msgstr ""

#: comments.php:31
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:58
msgid "Comments are closed."
msgstr ""

#: functions.php:104
msgid "Primary"
msgstr ""

#: functions.php:105
msgid "Social"
msgstr ""

#: functions.php:115
msgid "Sidebar"
msgstr ""

#: functions.php:117
msgid "Add widgets here."
msgstr ""

#: functions.php:125
msgid "Footer"
msgstr ""

#: functions.php:127
msgid "Widget area for the footer."
msgstr ""

#: header.php:24
msgid "Skip to content"
msgstr ""

#: inc/class-hide-page-title.php:51
msgid "Hide Page Title"
msgstr ""

#: inc/functions-comments.php:27
msgid "Pingback:"
msgstr ""

#: inc/functions-comments.php:27
msgid "Edit"
msgstr ""

#: inc/functions-comments.php:46
msgid "Author"
msgstr ""

#: inc/functions-comments.php:52
msgid "%s ago"
msgstr ""

#: inc/functions-comments.php:58
msgid "Your comment is awaiting moderation."
msgstr ""

#. translators: The &hellip; is the mark after the excerpt.
#: inc/functions-filters.php:31
msgid "&hellip;"
msgstr ""

#: inc/functions-icons.php:39
msgid "Please define an SVG icon filename."
msgstr ""

#: inc/functions-icons.php:44
msgid "Please define optional parameters in the form of an array."
msgstr ""

#: inc/functions-template-tags.php:104
msgctxt "no comments text"
msgid "No Comments"
msgstr ""

#: inc/functions-template-tags.php:105
msgctxt "single comment text"
msgid "1 Comment"
msgstr ""

#: inc/functions-template-tags.php:106
msgctxt "multiple comments text"
msgid "% Comments"
msgstr ""

#: inc/functions-template-tags.php:124, inc/functions-template-tags.php:125, inc/functions-template-tags.php:126
msgid "%s<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#: inc/functions-template-tags.php:128, inc/functions-template-tags.php:130
msgid "%s<span class=\"screen-reader-text\"> Comments on %s</span>"
msgstr ""

#: inc/functions-template-tags.php:129
msgid "%s<span class=\"screen-reader-text\"> Comment on %s</span>"
msgstr ""

#: inc/functions-template-tags.php:173
msgctxt "taxonomy terms separator"
msgid ", "
msgstr ""

#: inc/functions-template-tags.php:205
msgctxt "read more text"
msgid "Read more"
msgstr ""

#. translators: The $2%s is the post title shown to screen readers.
#: inc/functions-template-tags.php:212
msgid "%1$s %2$s"
msgstr ""

#. translators: 1 is the year, 2 is the site name.
#: inc/functions-template-tags.php:237
msgid "Copyright &#169; %1$s %2$s."
msgstr ""

#. translators: 1 is the theme name, 2 is theme author link.
#: inc/functions-template-tags.php:248
msgid "Theme: %1$s by %2$s."
msgstr ""

#: inc/functions-woocommerce.php:234
msgid "View your shopping cart"
msgstr ""

#: inc/functions-woocommerce.php:236
msgid "%d item"
msgid_plural "%d items"
msgstr[0] ""
msgstr[1] ""

#: partials/content-none.php:19
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: partials/content-none.php:25
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr ""

#: partials/content-page.php:24, partials/content-single-post.php:24, partials/content-single.php:23
msgid "Pages:"
msgstr ""

#: partials/content-page.php:28, partials/content-single-post.php:28, partials/content-single.php:27, partials/pagination-archive.php:15
msgid "Page"
msgstr ""

#: partials/content-single-post.php:35
msgid "Filed under: %s"
msgstr ""

#: partials/content-single-post.php:36
msgid "Tagged with: %s"
msgstr ""

#: partials/entry-byline.php:14
msgctxt "post author"
msgid "by %s"
msgstr ""

#: partials/page-header.php:17
msgid "Search Results for: %s"
msgstr ""

#: partials/page-header.php:19
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: partials/pagination-archive.php:13
msgctxt "posts navigation"
msgid "Previous"
msgstr ""

#: partials/pagination-archive.php:14
msgctxt "posts navigation"
msgid "Next"
msgstr ""

#: partials/pagination-single.php:13
msgctxt "previous post navigation"
msgid "Previous:"
msgstr ""

#: partials/pagination-single.php:14
msgctxt "next post navigation"
msgid "Next:"
msgstr ""

#: templates/sidebar-right.php:2
msgid "Sidebar Right"
msgstr ""
