<?php
/**
 * Template part for displaying single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tiptoe
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<?php get_template_part( 'partials/entry', 'byline' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages(
				array(
					'before'      => '<div class="page-links">' . esc_html__( 'Pages:', 'tiptoe' ),
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'tiptoe' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">,</span> ',
				)
			);
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
		tiptoe_post_terms(
			array(
				'taxonomy' => 'category',
				'text' => esc_html__( 'Filed under: %s', 'tiptoe' ),
			)
		);

		tiptoe_post_terms(
			array(
				'taxonomy' => 'post_tag',
				'text' => esc_html__( 'Tagged with: %s', 'tiptoe' ),
			)
		);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
