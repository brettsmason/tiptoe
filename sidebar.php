<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Tiptoe
 */

if ( ! tiptoe_display_sidebar() ) return;
if ( ! is_active_sidebar( 'sidebar-1' ) ) return;
?>

<aside id="sidebar-primary" class="sidebar-primary" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- .sidebar-primary -->
