<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tiptoe
 */

get_header(); ?>

	<main id="site-content" class="site-content" role="main">

		<?php
		if ( have_posts() ) :

			get_template_part( 'partials/page', 'header' );

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				* Include the Post-Type-specific template for the content.
				* If you want to override this in a child theme, then include a file
				* called content-___.php (where ___ is the Post Type name) and that will be used instead.
				*/
				get_template_part( 'content/content', get_post_type() );

			endwhile;

			get_template_part( 'partials/pagination', 'archive' );

		else :

			get_template_part( 'partials/content', 'none' );

		endif;
		?>

	</main><!-- #site-content -->

<?php
get_sidebar();
get_footer();
